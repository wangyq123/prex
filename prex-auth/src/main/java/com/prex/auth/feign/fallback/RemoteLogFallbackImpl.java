package com.prex.auth.feign.fallback;

import com.prex.auth.feign.RemoteUserService;
import com.prex.base.api.dto.UserDetailsInfo;
import com.prex.common.core.utils.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Classname RemoteLogFallbackImpl
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-24 10:56
 * @Version 1.0
 */
@Slf4j
@AllArgsConstructor
public class RemoteLogFallbackImpl implements RemoteUserService {

    private final Throwable throwable;

    @Override
    public R<UserDetailsInfo> info(String username) {
        log.error("feign 调用用户{},信息:{}", username, throwable.getLocalizedMessage());
        return null;
    }

    @Override
    public R<UserDetailsInfo> getUserInfoBySocial(String providerId, int providerUserId) {
        log.error("feign 调用用户{},信息:{}", providerUserId, throwable.getLocalizedMessage());
        return null;
    }
}
